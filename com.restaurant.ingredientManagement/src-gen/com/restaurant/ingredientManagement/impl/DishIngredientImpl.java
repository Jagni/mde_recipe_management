/**
 */
package com.restaurant.ingredientManagement.impl;

import com.restaurant.ingredientManagement.DishIngredient;
import com.restaurant.ingredientManagement.Ingredient;
import com.restaurant.ingredientManagement.IngredientManagementPackage;
import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Dish Ingredient</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link com.restaurant.ingredientManagement.impl.DishIngredientImpl#getQuantity <em>Quantity</em>}</li>
 *   <li>{@link com.restaurant.ingredientManagement.impl.DishIngredientImpl#getDishIngredient <em>Dish Ingredient</em>}</li>
 * </ul>
 *
 * @generated
 */
public class DishIngredientImpl extends MinimalEObjectImpl.Container implements DishIngredient {
	/**
	 * The default value of the '{@link #getQuantity() <em>Quantity</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getQuantity()
	 * @generated
	 * @ordered
	 */
	protected static final int QUANTITY_EDEFAULT = 0;

	/**
	 * The cached value of the '{@link #getQuantity() <em>Quantity</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getQuantity()
	 * @generated
	 * @ordered
	 */
	protected int quantity = QUANTITY_EDEFAULT;

	/**
	 * The cached value of the '{@link #getDishIngredient() <em>Dish Ingredient</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDishIngredient()
	 * @generated
	 * @ordered
	 */
	protected Ingredient dishIngredient;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected DishIngredientImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return IngredientManagementPackage.Literals.DISH_INGREDIENT;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public int getQuantity() {
		return quantity;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setQuantity(int newQuantity) {
		int oldQuantity = quantity;
		quantity = newQuantity;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, IngredientManagementPackage.DISH_INGREDIENT__QUANTITY,
					oldQuantity, quantity));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Ingredient getDishIngredient() {
		if (dishIngredient != null && dishIngredient.eIsProxy()) {
			InternalEObject oldDishIngredient = (InternalEObject) dishIngredient;
			dishIngredient = (Ingredient) eResolveProxy(oldDishIngredient);
			if (dishIngredient != oldDishIngredient) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE,
							IngredientManagementPackage.DISH_INGREDIENT__DISH_INGREDIENT, oldDishIngredient,
							dishIngredient));
			}
		}
		return dishIngredient;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Ingredient basicGetDishIngredient() {
		return dishIngredient;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setDishIngredient(Ingredient newDishIngredient) {
		Ingredient oldDishIngredient = dishIngredient;
		dishIngredient = newDishIngredient;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET,
					IngredientManagementPackage.DISH_INGREDIENT__DISH_INGREDIENT, oldDishIngredient, dishIngredient));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
		case IngredientManagementPackage.DISH_INGREDIENT__QUANTITY:
			return getQuantity();
		case IngredientManagementPackage.DISH_INGREDIENT__DISH_INGREDIENT:
			if (resolve)
				return getDishIngredient();
			return basicGetDishIngredient();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
		case IngredientManagementPackage.DISH_INGREDIENT__QUANTITY:
			setQuantity((Integer) newValue);
			return;
		case IngredientManagementPackage.DISH_INGREDIENT__DISH_INGREDIENT:
			setDishIngredient((Ingredient) newValue);
			return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
		case IngredientManagementPackage.DISH_INGREDIENT__QUANTITY:
			setQuantity(QUANTITY_EDEFAULT);
			return;
		case IngredientManagementPackage.DISH_INGREDIENT__DISH_INGREDIENT:
			setDishIngredient((Ingredient) null);
			return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
		case IngredientManagementPackage.DISH_INGREDIENT__QUANTITY:
			return quantity != QUANTITY_EDEFAULT;
		case IngredientManagementPackage.DISH_INGREDIENT__DISH_INGREDIENT:
			return dishIngredient != null;
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy())
			return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (quantity: ");
		result.append(quantity);
		result.append(')');
		return result.toString();
	}

} //DishIngredientImpl
