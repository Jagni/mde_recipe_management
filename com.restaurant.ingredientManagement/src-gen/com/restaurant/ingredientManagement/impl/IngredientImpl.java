/**
 */
package com.restaurant.ingredientManagement.impl;

import com.restaurant.ingredientManagement.Ingredient;
import com.restaurant.ingredientManagement.IngredientManagementPackage;
import com.restaurant.ingredientManagement.Measure;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.ecore.EClass;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Ingredient</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link com.restaurant.ingredientManagement.impl.IngredientImpl#getName <em>Name</em>}</li>
 *   <li>{@link com.restaurant.ingredientManagement.impl.IngredientImpl#getCalories <em>Calories</em>}</li>
 *   <li>{@link com.restaurant.ingredientManagement.impl.IngredientImpl#getCaloriesMeasure <em>Calories Measure</em>}</li>
 * </ul>
 *
 * @generated
 */
public class IngredientImpl extends MinimalEObjectImpl.Container implements Ingredient {
	/**
	 * The default value of the '{@link #getName() <em>Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getName()
	 * @generated
	 * @ordered
	 */
	protected static final String NAME_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getName() <em>Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getName()
	 * @generated
	 * @ordered
	 */
	protected String name = NAME_EDEFAULT;

	/**
	 * The default value of the '{@link #getCalories() <em>Calories</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCalories()
	 * @generated
	 * @ordered
	 */
	protected static final int CALORIES_EDEFAULT = 0;

	/**
	 * The cached value of the '{@link #getCalories() <em>Calories</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCalories()
	 * @generated
	 * @ordered
	 */
	protected int calories = CALORIES_EDEFAULT;

	/**
	 * The default value of the '{@link #getCaloriesMeasure() <em>Calories Measure</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCaloriesMeasure()
	 * @generated
	 * @ordered
	 */
	protected static final Measure CALORIES_MEASURE_EDEFAULT = Measure.UNIT;

	/**
	 * The cached value of the '{@link #getCaloriesMeasure() <em>Calories Measure</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCaloriesMeasure()
	 * @generated
	 * @ordered
	 */
	protected Measure caloriesMeasure = CALORIES_MEASURE_EDEFAULT;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected IngredientImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return IngredientManagementPackage.Literals.INGREDIENT;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getName() {
		return name;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setName(String newName) {
		String oldName = name;
		name = newName;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, IngredientManagementPackage.INGREDIENT__NAME, oldName,
					name));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public int getCalories() {
		return calories;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setCalories(int newCalories) {
		int oldCalories = calories;
		calories = newCalories;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, IngredientManagementPackage.INGREDIENT__CALORIES,
					oldCalories, calories));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Measure getCaloriesMeasure() {
		return caloriesMeasure;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setCaloriesMeasure(Measure newCaloriesMeasure) {
		Measure oldCaloriesMeasure = caloriesMeasure;
		caloriesMeasure = newCaloriesMeasure == null ? CALORIES_MEASURE_EDEFAULT : newCaloriesMeasure;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET,
					IngredientManagementPackage.INGREDIENT__CALORIES_MEASURE, oldCaloriesMeasure, caloriesMeasure));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
		case IngredientManagementPackage.INGREDIENT__NAME:
			return getName();
		case IngredientManagementPackage.INGREDIENT__CALORIES:
			return getCalories();
		case IngredientManagementPackage.INGREDIENT__CALORIES_MEASURE:
			return getCaloriesMeasure();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
		case IngredientManagementPackage.INGREDIENT__NAME:
			setName((String) newValue);
			return;
		case IngredientManagementPackage.INGREDIENT__CALORIES:
			setCalories((Integer) newValue);
			return;
		case IngredientManagementPackage.INGREDIENT__CALORIES_MEASURE:
			setCaloriesMeasure((Measure) newValue);
			return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
		case IngredientManagementPackage.INGREDIENT__NAME:
			setName(NAME_EDEFAULT);
			return;
		case IngredientManagementPackage.INGREDIENT__CALORIES:
			setCalories(CALORIES_EDEFAULT);
			return;
		case IngredientManagementPackage.INGREDIENT__CALORIES_MEASURE:
			setCaloriesMeasure(CALORIES_MEASURE_EDEFAULT);
			return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
		case IngredientManagementPackage.INGREDIENT__NAME:
			return NAME_EDEFAULT == null ? name != null : !NAME_EDEFAULT.equals(name);
		case IngredientManagementPackage.INGREDIENT__CALORIES:
			return calories != CALORIES_EDEFAULT;
		case IngredientManagementPackage.INGREDIENT__CALORIES_MEASURE:
			return caloriesMeasure != CALORIES_MEASURE_EDEFAULT;
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy())
			return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (name: ");
		result.append(name);
		result.append(", calories: ");
		result.append(calories);
		result.append(", caloriesMeasure: ");
		result.append(caloriesMeasure);
		result.append(')');
		return result.toString();
	}

} //IngredientImpl
