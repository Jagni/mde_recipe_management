/**
 */
package com.restaurant.ingredientManagement.impl;

import com.restaurant.ingredientManagement.Ingredient;
import com.restaurant.ingredientManagement.IngredientManagementPackage;
import com.restaurant.ingredientManagement.StockIngredient;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Stock Ingredient</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link com.restaurant.ingredientManagement.impl.StockIngredientImpl#getQuantity <em>Quantity</em>}</li>
 *   <li>{@link com.restaurant.ingredientManagement.impl.StockIngredientImpl#getStockIngredient <em>Stock Ingredient</em>}</li>
 * </ul>
 *
 * @generated
 */
public class StockIngredientImpl extends MinimalEObjectImpl.Container implements StockIngredient {
	/**
	 * The default value of the '{@link #getQuantity() <em>Quantity</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getQuantity()
	 * @generated
	 * @ordered
	 */
	protected static final int QUANTITY_EDEFAULT = 0;

	/**
	 * The cached value of the '{@link #getQuantity() <em>Quantity</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getQuantity()
	 * @generated
	 * @ordered
	 */
	protected int quantity = QUANTITY_EDEFAULT;

	/**
	 * The cached value of the '{@link #getStockIngredient() <em>Stock Ingredient</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getStockIngredient()
	 * @generated
	 * @ordered
	 */
	protected Ingredient stockIngredient;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected StockIngredientImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return IngredientManagementPackage.Literals.STOCK_INGREDIENT;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public int getQuantity() {
		return quantity;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setQuantity(int newQuantity) {
		int oldQuantity = quantity;
		quantity = newQuantity;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET,
					IngredientManagementPackage.STOCK_INGREDIENT__QUANTITY, oldQuantity, quantity));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Ingredient getStockIngredient() {
		if (stockIngredient != null && stockIngredient.eIsProxy()) {
			InternalEObject oldStockIngredient = (InternalEObject) stockIngredient;
			stockIngredient = (Ingredient) eResolveProxy(oldStockIngredient);
			if (stockIngredient != oldStockIngredient) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE,
							IngredientManagementPackage.STOCK_INGREDIENT__STOCK_INGREDIENT, oldStockIngredient,
							stockIngredient));
			}
		}
		return stockIngredient;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Ingredient basicGetStockIngredient() {
		return stockIngredient;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setStockIngredient(Ingredient newStockIngredient) {
		Ingredient oldStockIngredient = stockIngredient;
		stockIngredient = newStockIngredient;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET,
					IngredientManagementPackage.STOCK_INGREDIENT__STOCK_INGREDIENT, oldStockIngredient,
					stockIngredient));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
		case IngredientManagementPackage.STOCK_INGREDIENT__QUANTITY:
			return getQuantity();
		case IngredientManagementPackage.STOCK_INGREDIENT__STOCK_INGREDIENT:
			if (resolve)
				return getStockIngredient();
			return basicGetStockIngredient();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
		case IngredientManagementPackage.STOCK_INGREDIENT__QUANTITY:
			setQuantity((Integer) newValue);
			return;
		case IngredientManagementPackage.STOCK_INGREDIENT__STOCK_INGREDIENT:
			setStockIngredient((Ingredient) newValue);
			return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
		case IngredientManagementPackage.STOCK_INGREDIENT__QUANTITY:
			setQuantity(QUANTITY_EDEFAULT);
			return;
		case IngredientManagementPackage.STOCK_INGREDIENT__STOCK_INGREDIENT:
			setStockIngredient((Ingredient) null);
			return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
		case IngredientManagementPackage.STOCK_INGREDIENT__QUANTITY:
			return quantity != QUANTITY_EDEFAULT;
		case IngredientManagementPackage.STOCK_INGREDIENT__STOCK_INGREDIENT:
			return stockIngredient != null;
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy())
			return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (quantity: ");
		result.append(quantity);
		result.append(')');
		return result.toString();
	}

} //StockIngredientImpl
