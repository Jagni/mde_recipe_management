/**
 */
package com.restaurant.ingredientManagement.impl;

import com.restaurant.ingredientManagement.Dish;
import com.restaurant.ingredientManagement.Ingredient;
import com.restaurant.ingredientManagement.IngredientManagementPackage;
import com.restaurant.ingredientManagement.Restaurant;
import com.restaurant.ingredientManagement.StockIngredient;

import java.util.Collection;

import java.util.Date;
import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Restaurant</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link com.restaurant.ingredientManagement.impl.RestaurantImpl#getName <em>Name</em>}</li>
 *   <li>{@link com.restaurant.ingredientManagement.impl.RestaurantImpl#getDishes <em>Dishes</em>}</li>
 *   <li>{@link com.restaurant.ingredientManagement.impl.RestaurantImpl#getStockIngredients <em>Stock Ingredients</em>}</li>
 *   <li>{@link com.restaurant.ingredientManagement.impl.RestaurantImpl#getIngredients <em>Ingredients</em>}</li>
 *   <li>{@link com.restaurant.ingredientManagement.impl.RestaurantImpl#getGenerationDate <em>Generation Date</em>}</li>
 *   <li>{@link com.restaurant.ingredientManagement.impl.RestaurantImpl#getSlogan <em>Slogan</em>}</li>
 * </ul>
 *
 * @generated
 */
public class RestaurantImpl extends MinimalEObjectImpl.Container implements Restaurant {
	/**
	 * The default value of the '{@link #getName() <em>Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getName()
	 * @generated
	 * @ordered
	 */
	protected static final String NAME_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getName() <em>Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getName()
	 * @generated
	 * @ordered
	 */
	protected String name = NAME_EDEFAULT;

	/**
	 * The cached value of the '{@link #getDishes() <em>Dishes</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDishes()
	 * @generated
	 * @ordered
	 */
	protected EList<Dish> dishes;

	/**
	 * The cached value of the '{@link #getStockIngredients() <em>Stock Ingredients</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getStockIngredients()
	 * @generated
	 * @ordered
	 */
	protected EList<StockIngredient> stockIngredients;

	/**
	 * The cached value of the '{@link #getIngredients() <em>Ingredients</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getIngredients()
	 * @generated
	 * @ordered
	 */
	protected EList<Ingredient> ingredients;

	/**
	 * The default value of the '{@link #getGenerationDate() <em>Generation Date</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getGenerationDate()
	 * @generated
	 * @ordered
	 */
	protected static final Date GENERATION_DATE_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getGenerationDate() <em>Generation Date</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getGenerationDate()
	 * @generated
	 * @ordered
	 */
	protected Date generationDate = GENERATION_DATE_EDEFAULT;

	/**
	 * The default value of the '{@link #getSlogan() <em>Slogan</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSlogan()
	 * @generated
	 * @ordered
	 */
	protected static final String SLOGAN_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getSlogan() <em>Slogan</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSlogan()
	 * @generated
	 * @ordered
	 */
	protected String slogan = SLOGAN_EDEFAULT;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected RestaurantImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return IngredientManagementPackage.Literals.RESTAURANT;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getName() {
		return name;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setName(String newName) {
		String oldName = name;
		name = newName;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, IngredientManagementPackage.RESTAURANT__NAME, oldName,
					name));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<Dish> getDishes() {
		if (dishes == null) {
			dishes = new EObjectContainmentEList<Dish>(Dish.class, this,
					IngredientManagementPackage.RESTAURANT__DISHES);
		}
		return dishes;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<StockIngredient> getStockIngredients() {
		if (stockIngredients == null) {
			stockIngredients = new EObjectContainmentEList<StockIngredient>(StockIngredient.class, this,
					IngredientManagementPackage.RESTAURANT__STOCK_INGREDIENTS);
		}
		return stockIngredients;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<Ingredient> getIngredients() {
		if (ingredients == null) {
			ingredients = new EObjectContainmentEList<Ingredient>(Ingredient.class, this,
					IngredientManagementPackage.RESTAURANT__INGREDIENTS);
		}
		return ingredients;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Date getGenerationDate() {
		return generationDate;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setGenerationDate(Date newGenerationDate) {
		Date oldGenerationDate = generationDate;
		generationDate = newGenerationDate;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET,
					IngredientManagementPackage.RESTAURANT__GENERATION_DATE, oldGenerationDate, generationDate));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getSlogan() {
		return slogan;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setSlogan(String newSlogan) {
		String oldSlogan = slogan;
		slogan = newSlogan;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, IngredientManagementPackage.RESTAURANT__SLOGAN,
					oldSlogan, slogan));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
		case IngredientManagementPackage.RESTAURANT__DISHES:
			return ((InternalEList<?>) getDishes()).basicRemove(otherEnd, msgs);
		case IngredientManagementPackage.RESTAURANT__STOCK_INGREDIENTS:
			return ((InternalEList<?>) getStockIngredients()).basicRemove(otherEnd, msgs);
		case IngredientManagementPackage.RESTAURANT__INGREDIENTS:
			return ((InternalEList<?>) getIngredients()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
		case IngredientManagementPackage.RESTAURANT__NAME:
			return getName();
		case IngredientManagementPackage.RESTAURANT__DISHES:
			return getDishes();
		case IngredientManagementPackage.RESTAURANT__STOCK_INGREDIENTS:
			return getStockIngredients();
		case IngredientManagementPackage.RESTAURANT__INGREDIENTS:
			return getIngredients();
		case IngredientManagementPackage.RESTAURANT__GENERATION_DATE:
			return getGenerationDate();
		case IngredientManagementPackage.RESTAURANT__SLOGAN:
			return getSlogan();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
		case IngredientManagementPackage.RESTAURANT__NAME:
			setName((String) newValue);
			return;
		case IngredientManagementPackage.RESTAURANT__DISHES:
			getDishes().clear();
			getDishes().addAll((Collection<? extends Dish>) newValue);
			return;
		case IngredientManagementPackage.RESTAURANT__STOCK_INGREDIENTS:
			getStockIngredients().clear();
			getStockIngredients().addAll((Collection<? extends StockIngredient>) newValue);
			return;
		case IngredientManagementPackage.RESTAURANT__INGREDIENTS:
			getIngredients().clear();
			getIngredients().addAll((Collection<? extends Ingredient>) newValue);
			return;
		case IngredientManagementPackage.RESTAURANT__GENERATION_DATE:
			setGenerationDate((Date) newValue);
			return;
		case IngredientManagementPackage.RESTAURANT__SLOGAN:
			setSlogan((String) newValue);
			return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
		case IngredientManagementPackage.RESTAURANT__NAME:
			setName(NAME_EDEFAULT);
			return;
		case IngredientManagementPackage.RESTAURANT__DISHES:
			getDishes().clear();
			return;
		case IngredientManagementPackage.RESTAURANT__STOCK_INGREDIENTS:
			getStockIngredients().clear();
			return;
		case IngredientManagementPackage.RESTAURANT__INGREDIENTS:
			getIngredients().clear();
			return;
		case IngredientManagementPackage.RESTAURANT__GENERATION_DATE:
			setGenerationDate(GENERATION_DATE_EDEFAULT);
			return;
		case IngredientManagementPackage.RESTAURANT__SLOGAN:
			setSlogan(SLOGAN_EDEFAULT);
			return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
		case IngredientManagementPackage.RESTAURANT__NAME:
			return NAME_EDEFAULT == null ? name != null : !NAME_EDEFAULT.equals(name);
		case IngredientManagementPackage.RESTAURANT__DISHES:
			return dishes != null && !dishes.isEmpty();
		case IngredientManagementPackage.RESTAURANT__STOCK_INGREDIENTS:
			return stockIngredients != null && !stockIngredients.isEmpty();
		case IngredientManagementPackage.RESTAURANT__INGREDIENTS:
			return ingredients != null && !ingredients.isEmpty();
		case IngredientManagementPackage.RESTAURANT__GENERATION_DATE:
			return GENERATION_DATE_EDEFAULT == null ? generationDate != null
					: !GENERATION_DATE_EDEFAULT.equals(generationDate);
		case IngredientManagementPackage.RESTAURANT__SLOGAN:
			return SLOGAN_EDEFAULT == null ? slogan != null : !SLOGAN_EDEFAULT.equals(slogan);
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy())
			return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (name: ");
		result.append(name);
		result.append(", generationDate: ");
		result.append(generationDate);
		result.append(", slogan: ");
		result.append(slogan);
		result.append(')');
		return result.toString();
	}

} //RestaurantImpl
