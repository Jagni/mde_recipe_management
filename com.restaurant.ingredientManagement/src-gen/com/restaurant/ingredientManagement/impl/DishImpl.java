/**
 */
package com.restaurant.ingredientManagement.impl;

import com.restaurant.ingredientManagement.Dish;
import com.restaurant.ingredientManagement.DishIngredient;
import com.restaurant.ingredientManagement.IngredientManagementPackage;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Dish</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link com.restaurant.ingredientManagement.impl.DishImpl#getDishIngredients <em>Dish Ingredients</em>}</li>
 *   <li>{@link com.restaurant.ingredientManagement.impl.DishImpl#getQuantity <em>Quantity</em>}</li>
 *   <li>{@link com.restaurant.ingredientManagement.impl.DishImpl#getNumberOfPeopleServed <em>Number Of People Served</em>}</li>
 *   <li>{@link com.restaurant.ingredientManagement.impl.DishImpl#getName <em>Name</em>}</li>
 * </ul>
 *
 * @generated
 */
public class DishImpl extends MinimalEObjectImpl.Container implements Dish {
	/**
	 * The cached value of the '{@link #getDishIngredients() <em>Dish Ingredients</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDishIngredients()
	 * @generated
	 * @ordered
	 */
	protected EList<DishIngredient> dishIngredients;

	/**
	 * The default value of the '{@link #getQuantity() <em>Quantity</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getQuantity()
	 * @generated
	 * @ordered
	 */
	protected static final int QUANTITY_EDEFAULT = 0;

	/**
	 * The cached value of the '{@link #getQuantity() <em>Quantity</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getQuantity()
	 * @generated
	 * @ordered
	 */
	protected int quantity = QUANTITY_EDEFAULT;

	/**
	 * The default value of the '{@link #getNumberOfPeopleServed() <em>Number Of People Served</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getNumberOfPeopleServed()
	 * @generated
	 * @ordered
	 */
	protected static final int NUMBER_OF_PEOPLE_SERVED_EDEFAULT = 0;

	/**
	 * The cached value of the '{@link #getNumberOfPeopleServed() <em>Number Of People Served</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getNumberOfPeopleServed()
	 * @generated
	 * @ordered
	 */
	protected int numberOfPeopleServed = NUMBER_OF_PEOPLE_SERVED_EDEFAULT;

	/**
	 * The default value of the '{@link #getName() <em>Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getName()
	 * @generated
	 * @ordered
	 */
	protected static final String NAME_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getName() <em>Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getName()
	 * @generated
	 * @ordered
	 */
	protected String name = NAME_EDEFAULT;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected DishImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return IngredientManagementPackage.Literals.DISH;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<DishIngredient> getDishIngredients() {
		if (dishIngredients == null) {
			dishIngredients = new EObjectContainmentEList<DishIngredient>(DishIngredient.class, this,
					IngredientManagementPackage.DISH__DISH_INGREDIENTS);
		}
		return dishIngredients;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public int getQuantity() {
		return quantity;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setQuantity(int newQuantity) {
		int oldQuantity = quantity;
		quantity = newQuantity;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, IngredientManagementPackage.DISH__QUANTITY,
					oldQuantity, quantity));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public int getNumberOfPeopleServed() {
		return numberOfPeopleServed;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setNumberOfPeopleServed(int newNumberOfPeopleServed) {
		int oldNumberOfPeopleServed = numberOfPeopleServed;
		numberOfPeopleServed = newNumberOfPeopleServed;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET,
					IngredientManagementPackage.DISH__NUMBER_OF_PEOPLE_SERVED, oldNumberOfPeopleServed,
					numberOfPeopleServed));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getName() {
		return name;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setName(String newName) {
		String oldName = name;
		name = newName;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, IngredientManagementPackage.DISH__NAME, oldName,
					name));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
		case IngredientManagementPackage.DISH__DISH_INGREDIENTS:
			return ((InternalEList<?>) getDishIngredients()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
		case IngredientManagementPackage.DISH__DISH_INGREDIENTS:
			return getDishIngredients();
		case IngredientManagementPackage.DISH__QUANTITY:
			return getQuantity();
		case IngredientManagementPackage.DISH__NUMBER_OF_PEOPLE_SERVED:
			return getNumberOfPeopleServed();
		case IngredientManagementPackage.DISH__NAME:
			return getName();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
		case IngredientManagementPackage.DISH__DISH_INGREDIENTS:
			getDishIngredients().clear();
			getDishIngredients().addAll((Collection<? extends DishIngredient>) newValue);
			return;
		case IngredientManagementPackage.DISH__QUANTITY:
			setQuantity((Integer) newValue);
			return;
		case IngredientManagementPackage.DISH__NUMBER_OF_PEOPLE_SERVED:
			setNumberOfPeopleServed((Integer) newValue);
			return;
		case IngredientManagementPackage.DISH__NAME:
			setName((String) newValue);
			return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
		case IngredientManagementPackage.DISH__DISH_INGREDIENTS:
			getDishIngredients().clear();
			return;
		case IngredientManagementPackage.DISH__QUANTITY:
			setQuantity(QUANTITY_EDEFAULT);
			return;
		case IngredientManagementPackage.DISH__NUMBER_OF_PEOPLE_SERVED:
			setNumberOfPeopleServed(NUMBER_OF_PEOPLE_SERVED_EDEFAULT);
			return;
		case IngredientManagementPackage.DISH__NAME:
			setName(NAME_EDEFAULT);
			return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
		case IngredientManagementPackage.DISH__DISH_INGREDIENTS:
			return dishIngredients != null && !dishIngredients.isEmpty();
		case IngredientManagementPackage.DISH__QUANTITY:
			return quantity != QUANTITY_EDEFAULT;
		case IngredientManagementPackage.DISH__NUMBER_OF_PEOPLE_SERVED:
			return numberOfPeopleServed != NUMBER_OF_PEOPLE_SERVED_EDEFAULT;
		case IngredientManagementPackage.DISH__NAME:
			return NAME_EDEFAULT == null ? name != null : !NAME_EDEFAULT.equals(name);
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy())
			return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (quantity: ");
		result.append(quantity);
		result.append(", numberOfPeopleServed: ");
		result.append(numberOfPeopleServed);
		result.append(", name: ");
		result.append(name);
		result.append(')');
		return result.toString();
	}

} //DishImpl
