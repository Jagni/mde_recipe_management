/**
 */
package com.restaurant.ingredientManagement;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Dish Ingredient</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link com.restaurant.ingredientManagement.DishIngredient#getQuantity <em>Quantity</em>}</li>
 *   <li>{@link com.restaurant.ingredientManagement.DishIngredient#getDishIngredient <em>Dish Ingredient</em>}</li>
 * </ul>
 *
 * @see com.restaurant.ingredientManagement.IngredientManagementPackage#getDishIngredient()
 * @model
 * @generated
 */
public interface DishIngredient extends EObject {
	/**
	 * Returns the value of the '<em><b>Quantity</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Quantity</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Quantity</em>' attribute.
	 * @see #setQuantity(int)
	 * @see com.restaurant.ingredientManagement.IngredientManagementPackage#getDishIngredient_Quantity()
	 * @model
	 * @generated
	 */
	int getQuantity();

	/**
	 * Sets the value of the '{@link com.restaurant.ingredientManagement.DishIngredient#getQuantity <em>Quantity</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Quantity</em>' attribute.
	 * @see #getQuantity()
	 * @generated
	 */
	void setQuantity(int value);

	/**
	 * Returns the value of the '<em><b>Dish Ingredient</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Dish Ingredient</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Dish Ingredient</em>' reference.
	 * @see #setDishIngredient(Ingredient)
	 * @see com.restaurant.ingredientManagement.IngredientManagementPackage#getDishIngredient_DishIngredient()
	 * @model required="true"
	 * @generated
	 */
	Ingredient getDishIngredient();

	/**
	 * Sets the value of the '{@link com.restaurant.ingredientManagement.DishIngredient#getDishIngredient <em>Dish Ingredient</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Dish Ingredient</em>' reference.
	 * @see #getDishIngredient()
	 * @generated
	 */
	void setDishIngredient(Ingredient value);

} // DishIngredient
