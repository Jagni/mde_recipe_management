/**
 */
package com.restaurant.ingredientManagement;

import java.util.Date;
import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Restaurant</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link com.restaurant.ingredientManagement.Restaurant#getName <em>Name</em>}</li>
 *   <li>{@link com.restaurant.ingredientManagement.Restaurant#getDishes <em>Dishes</em>}</li>
 *   <li>{@link com.restaurant.ingredientManagement.Restaurant#getStockIngredients <em>Stock Ingredients</em>}</li>
 *   <li>{@link com.restaurant.ingredientManagement.Restaurant#getIngredients <em>Ingredients</em>}</li>
 *   <li>{@link com.restaurant.ingredientManagement.Restaurant#getGenerationDate <em>Generation Date</em>}</li>
 *   <li>{@link com.restaurant.ingredientManagement.Restaurant#getSlogan <em>Slogan</em>}</li>
 * </ul>
 *
 * @see com.restaurant.ingredientManagement.IngredientManagementPackage#getRestaurant()
 * @model
 * @generated
 */
public interface Restaurant extends EObject {
	/**
	 * Returns the value of the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Name</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Name</em>' attribute.
	 * @see #setName(String)
	 * @see com.restaurant.ingredientManagement.IngredientManagementPackage#getRestaurant_Name()
	 * @model
	 * @generated
	 */
	String getName();

	/**
	 * Sets the value of the '{@link com.restaurant.ingredientManagement.Restaurant#getName <em>Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Name</em>' attribute.
	 * @see #getName()
	 * @generated
	 */
	void setName(String value);

	/**
	 * Returns the value of the '<em><b>Dishes</b></em>' containment reference list.
	 * The list contents are of type {@link com.restaurant.ingredientManagement.Dish}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Dishes</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Dishes</em>' containment reference list.
	 * @see com.restaurant.ingredientManagement.IngredientManagementPackage#getRestaurant_Dishes()
	 * @model containment="true"
	 * @generated
	 */
	EList<Dish> getDishes();

	/**
	 * Returns the value of the '<em><b>Stock Ingredients</b></em>' containment reference list.
	 * The list contents are of type {@link com.restaurant.ingredientManagement.StockIngredient}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Stock Ingredients</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Stock Ingredients</em>' containment reference list.
	 * @see com.restaurant.ingredientManagement.IngredientManagementPackage#getRestaurant_StockIngredients()
	 * @model containment="true"
	 * @generated
	 */
	EList<StockIngredient> getStockIngredients();

	/**
	 * Returns the value of the '<em><b>Ingredients</b></em>' containment reference list.
	 * The list contents are of type {@link com.restaurant.ingredientManagement.Ingredient}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Ingredients</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Ingredients</em>' containment reference list.
	 * @see com.restaurant.ingredientManagement.IngredientManagementPackage#getRestaurant_Ingredients()
	 * @model containment="true"
	 * @generated
	 */
	EList<Ingredient> getIngredients();

	/**
	 * Returns the value of the '<em><b>Generation Date</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Generation Date</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Generation Date</em>' attribute.
	 * @see #setGenerationDate(Date)
	 * @see com.restaurant.ingredientManagement.IngredientManagementPackage#getRestaurant_GenerationDate()
	 * @model
	 * @generated
	 */
	Date getGenerationDate();

	/**
	 * Sets the value of the '{@link com.restaurant.ingredientManagement.Restaurant#getGenerationDate <em>Generation Date</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Generation Date</em>' attribute.
	 * @see #getGenerationDate()
	 * @generated
	 */
	void setGenerationDate(Date value);

	/**
	 * Returns the value of the '<em><b>Slogan</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Slogan</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Slogan</em>' attribute.
	 * @see #setSlogan(String)
	 * @see com.restaurant.ingredientManagement.IngredientManagementPackage#getRestaurant_Slogan()
	 * @model
	 * @generated
	 */
	String getSlogan();

	/**
	 * Sets the value of the '{@link com.restaurant.ingredientManagement.Restaurant#getSlogan <em>Slogan</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Slogan</em>' attribute.
	 * @see #getSlogan()
	 * @generated
	 */
	void setSlogan(String value);

} // Restaurant
