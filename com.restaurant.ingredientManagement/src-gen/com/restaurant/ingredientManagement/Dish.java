/**
 */
package com.restaurant.ingredientManagement;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Dish</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link com.restaurant.ingredientManagement.Dish#getDishIngredients <em>Dish Ingredients</em>}</li>
 *   <li>{@link com.restaurant.ingredientManagement.Dish#getQuantity <em>Quantity</em>}</li>
 *   <li>{@link com.restaurant.ingredientManagement.Dish#getNumberOfPeopleServed <em>Number Of People Served</em>}</li>
 *   <li>{@link com.restaurant.ingredientManagement.Dish#getName <em>Name</em>}</li>
 * </ul>
 *
 * @see com.restaurant.ingredientManagement.IngredientManagementPackage#getDish()
 * @model
 * @generated
 */
public interface Dish extends EObject {
	/**
	 * Returns the value of the '<em><b>Dish Ingredients</b></em>' containment reference list.
	 * The list contents are of type {@link com.restaurant.ingredientManagement.DishIngredient}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Dish Ingredients</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Dish Ingredients</em>' containment reference list.
	 * @see com.restaurant.ingredientManagement.IngredientManagementPackage#getDish_DishIngredients()
	 * @model containment="true" required="true"
	 * @generated
	 */
	EList<DishIngredient> getDishIngredients();

	/**
	 * Returns the value of the '<em><b>Quantity</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Quantity</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Quantity</em>' attribute.
	 * @see #setQuantity(int)
	 * @see com.restaurant.ingredientManagement.IngredientManagementPackage#getDish_Quantity()
	 * @model
	 * @generated
	 */
	int getQuantity();

	/**
	 * Sets the value of the '{@link com.restaurant.ingredientManagement.Dish#getQuantity <em>Quantity</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Quantity</em>' attribute.
	 * @see #getQuantity()
	 * @generated
	 */
	void setQuantity(int value);

	/**
	 * Returns the value of the '<em><b>Number Of People Served</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Number Of People Served</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Number Of People Served</em>' attribute.
	 * @see #setNumberOfPeopleServed(int)
	 * @see com.restaurant.ingredientManagement.IngredientManagementPackage#getDish_NumberOfPeopleServed()
	 * @model
	 * @generated
	 */
	int getNumberOfPeopleServed();

	/**
	 * Sets the value of the '{@link com.restaurant.ingredientManagement.Dish#getNumberOfPeopleServed <em>Number Of People Served</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Number Of People Served</em>' attribute.
	 * @see #getNumberOfPeopleServed()
	 * @generated
	 */
	void setNumberOfPeopleServed(int value);

	/**
	 * Returns the value of the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Name</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Name</em>' attribute.
	 * @see #setName(String)
	 * @see com.restaurant.ingredientManagement.IngredientManagementPackage#getDish_Name()
	 * @model
	 * @generated
	 */
	String getName();

	/**
	 * Sets the value of the '{@link com.restaurant.ingredientManagement.Dish#getName <em>Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Name</em>' attribute.
	 * @see #getName()
	 * @generated
	 */
	void setName(String value);

} // Dish
