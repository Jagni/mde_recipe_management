/**
 */
package com.restaurant.ingredientManagement;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Stock Ingredient</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link com.restaurant.ingredientManagement.StockIngredient#getQuantity <em>Quantity</em>}</li>
 *   <li>{@link com.restaurant.ingredientManagement.StockIngredient#getStockIngredient <em>Stock Ingredient</em>}</li>
 * </ul>
 *
 * @see com.restaurant.ingredientManagement.IngredientManagementPackage#getStockIngredient()
 * @model
 * @generated
 */
public interface StockIngredient extends EObject {
	/**
	 * Returns the value of the '<em><b>Quantity</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Quantity</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Quantity</em>' attribute.
	 * @see #setQuantity(int)
	 * @see com.restaurant.ingredientManagement.IngredientManagementPackage#getStockIngredient_Quantity()
	 * @model
	 * @generated
	 */
	int getQuantity();

	/**
	 * Sets the value of the '{@link com.restaurant.ingredientManagement.StockIngredient#getQuantity <em>Quantity</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Quantity</em>' attribute.
	 * @see #getQuantity()
	 * @generated
	 */
	void setQuantity(int value);

	/**
	 * Returns the value of the '<em><b>Stock Ingredient</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Stock Ingredient</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Stock Ingredient</em>' reference.
	 * @see #setStockIngredient(Ingredient)
	 * @see com.restaurant.ingredientManagement.IngredientManagementPackage#getStockIngredient_StockIngredient()
	 * @model required="true"
	 * @generated
	 */
	Ingredient getStockIngredient();

	/**
	 * Sets the value of the '{@link com.restaurant.ingredientManagement.StockIngredient#getStockIngredient <em>Stock Ingredient</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Stock Ingredient</em>' reference.
	 * @see #getStockIngredient()
	 * @generated
	 */
	void setStockIngredient(Ingredient value);

} // StockIngredient
