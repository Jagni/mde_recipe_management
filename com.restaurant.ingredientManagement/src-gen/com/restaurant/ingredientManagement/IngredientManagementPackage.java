/**
 */
package com.restaurant.ingredientManagement;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EEnum;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;

/**
 * <!-- begin-user-doc -->
 * The <b>Package</b> for the model.
 * It contains accessors for the meta objects to represent
 * <ul>
 *   <li>each class,</li>
 *   <li>each feature of each class,</li>
 *   <li>each operation of each class,</li>
 *   <li>each enum,</li>
 *   <li>and each data type</li>
 * </ul>
 * <!-- end-user-doc -->
 * @see com.restaurant.ingredientManagement.IngredientManagementFactory
 * @model kind="package"
 * @generated
 */
public interface IngredientManagementPackage extends EPackage {
	/**
	 * The package name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNAME = "ingredientManagement";

	/**
	 * The package namespace URI.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_URI = "http://www.example.org/ingredientManagement";

	/**
	 * The package namespace name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_PREFIX = "ingredientManagement";

	/**
	 * The singleton instance of the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	IngredientManagementPackage eINSTANCE = com.restaurant.ingredientManagement.impl.IngredientManagementPackageImpl
			.init();

	/**
	 * The meta object id for the '{@link com.restaurant.ingredientManagement.impl.StockIngredientImpl <em>Stock Ingredient</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see com.restaurant.ingredientManagement.impl.StockIngredientImpl
	 * @see com.restaurant.ingredientManagement.impl.IngredientManagementPackageImpl#getStockIngredient()
	 * @generated
	 */
	int STOCK_INGREDIENT = 0;

	/**
	 * The feature id for the '<em><b>Quantity</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STOCK_INGREDIENT__QUANTITY = 0;

	/**
	 * The feature id for the '<em><b>Stock Ingredient</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STOCK_INGREDIENT__STOCK_INGREDIENT = 1;

	/**
	 * The number of structural features of the '<em>Stock Ingredient</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STOCK_INGREDIENT_FEATURE_COUNT = 2;

	/**
	 * The number of operations of the '<em>Stock Ingredient</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STOCK_INGREDIENT_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link com.restaurant.ingredientManagement.impl.IngredientImpl <em>Ingredient</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see com.restaurant.ingredientManagement.impl.IngredientImpl
	 * @see com.restaurant.ingredientManagement.impl.IngredientManagementPackageImpl#getIngredient()
	 * @generated
	 */
	int INGREDIENT = 1;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INGREDIENT__NAME = 0;

	/**
	 * The feature id for the '<em><b>Calories</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INGREDIENT__CALORIES = 1;

	/**
	 * The feature id for the '<em><b>Calories Measure</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INGREDIENT__CALORIES_MEASURE = 2;

	/**
	 * The number of structural features of the '<em>Ingredient</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INGREDIENT_FEATURE_COUNT = 3;

	/**
	 * The number of operations of the '<em>Ingredient</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INGREDIENT_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link com.restaurant.ingredientManagement.impl.DishIngredientImpl <em>Dish Ingredient</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see com.restaurant.ingredientManagement.impl.DishIngredientImpl
	 * @see com.restaurant.ingredientManagement.impl.IngredientManagementPackageImpl#getDishIngredient()
	 * @generated
	 */
	int DISH_INGREDIENT = 2;

	/**
	 * The feature id for the '<em><b>Quantity</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DISH_INGREDIENT__QUANTITY = 0;

	/**
	 * The feature id for the '<em><b>Dish Ingredient</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DISH_INGREDIENT__DISH_INGREDIENT = 1;

	/**
	 * The number of structural features of the '<em>Dish Ingredient</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DISH_INGREDIENT_FEATURE_COUNT = 2;

	/**
	 * The number of operations of the '<em>Dish Ingredient</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DISH_INGREDIENT_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link com.restaurant.ingredientManagement.impl.DishImpl <em>Dish</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see com.restaurant.ingredientManagement.impl.DishImpl
	 * @see com.restaurant.ingredientManagement.impl.IngredientManagementPackageImpl#getDish()
	 * @generated
	 */
	int DISH = 3;

	/**
	 * The feature id for the '<em><b>Dish Ingredients</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DISH__DISH_INGREDIENTS = 0;

	/**
	 * The feature id for the '<em><b>Quantity</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DISH__QUANTITY = 1;

	/**
	 * The feature id for the '<em><b>Number Of People Served</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DISH__NUMBER_OF_PEOPLE_SERVED = 2;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DISH__NAME = 3;

	/**
	 * The number of structural features of the '<em>Dish</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DISH_FEATURE_COUNT = 4;

	/**
	 * The number of operations of the '<em>Dish</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DISH_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link com.restaurant.ingredientManagement.impl.RestaurantImpl <em>Restaurant</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see com.restaurant.ingredientManagement.impl.RestaurantImpl
	 * @see com.restaurant.ingredientManagement.impl.IngredientManagementPackageImpl#getRestaurant()
	 * @generated
	 */
	int RESTAURANT = 4;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RESTAURANT__NAME = 0;

	/**
	 * The feature id for the '<em><b>Dishes</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RESTAURANT__DISHES = 1;

	/**
	 * The feature id for the '<em><b>Stock Ingredients</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RESTAURANT__STOCK_INGREDIENTS = 2;

	/**
	 * The feature id for the '<em><b>Ingredients</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RESTAURANT__INGREDIENTS = 3;

	/**
	 * The feature id for the '<em><b>Generation Date</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RESTAURANT__GENERATION_DATE = 4;

	/**
	 * The feature id for the '<em><b>Slogan</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RESTAURANT__SLOGAN = 5;

	/**
	 * The number of structural features of the '<em>Restaurant</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RESTAURANT_FEATURE_COUNT = 6;

	/**
	 * The number of operations of the '<em>Restaurant</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RESTAURANT_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link com.restaurant.ingredientManagement.Measure <em>Measure</em>}' enum.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see com.restaurant.ingredientManagement.Measure
	 * @see com.restaurant.ingredientManagement.impl.IngredientManagementPackageImpl#getMeasure()
	 * @generated
	 */
	int MEASURE = 5;

	/**
	 * Returns the meta object for class '{@link com.restaurant.ingredientManagement.StockIngredient <em>Stock Ingredient</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Stock Ingredient</em>'.
	 * @see com.restaurant.ingredientManagement.StockIngredient
	 * @generated
	 */
	EClass getStockIngredient();

	/**
	 * Returns the meta object for the attribute '{@link com.restaurant.ingredientManagement.StockIngredient#getQuantity <em>Quantity</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Quantity</em>'.
	 * @see com.restaurant.ingredientManagement.StockIngredient#getQuantity()
	 * @see #getStockIngredient()
	 * @generated
	 */
	EAttribute getStockIngredient_Quantity();

	/**
	 * Returns the meta object for the reference '{@link com.restaurant.ingredientManagement.StockIngredient#getStockIngredient <em>Stock Ingredient</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Stock Ingredient</em>'.
	 * @see com.restaurant.ingredientManagement.StockIngredient#getStockIngredient()
	 * @see #getStockIngredient()
	 * @generated
	 */
	EReference getStockIngredient_StockIngredient();

	/**
	 * Returns the meta object for class '{@link com.restaurant.ingredientManagement.Ingredient <em>Ingredient</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Ingredient</em>'.
	 * @see com.restaurant.ingredientManagement.Ingredient
	 * @generated
	 */
	EClass getIngredient();

	/**
	 * Returns the meta object for the attribute '{@link com.restaurant.ingredientManagement.Ingredient#getName <em>Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Name</em>'.
	 * @see com.restaurant.ingredientManagement.Ingredient#getName()
	 * @see #getIngredient()
	 * @generated
	 */
	EAttribute getIngredient_Name();

	/**
	 * Returns the meta object for the attribute '{@link com.restaurant.ingredientManagement.Ingredient#getCalories <em>Calories</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Calories</em>'.
	 * @see com.restaurant.ingredientManagement.Ingredient#getCalories()
	 * @see #getIngredient()
	 * @generated
	 */
	EAttribute getIngredient_Calories();

	/**
	 * Returns the meta object for the attribute '{@link com.restaurant.ingredientManagement.Ingredient#getCaloriesMeasure <em>Calories Measure</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Calories Measure</em>'.
	 * @see com.restaurant.ingredientManagement.Ingredient#getCaloriesMeasure()
	 * @see #getIngredient()
	 * @generated
	 */
	EAttribute getIngredient_CaloriesMeasure();

	/**
	 * Returns the meta object for class '{@link com.restaurant.ingredientManagement.DishIngredient <em>Dish Ingredient</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Dish Ingredient</em>'.
	 * @see com.restaurant.ingredientManagement.DishIngredient
	 * @generated
	 */
	EClass getDishIngredient();

	/**
	 * Returns the meta object for the attribute '{@link com.restaurant.ingredientManagement.DishIngredient#getQuantity <em>Quantity</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Quantity</em>'.
	 * @see com.restaurant.ingredientManagement.DishIngredient#getQuantity()
	 * @see #getDishIngredient()
	 * @generated
	 */
	EAttribute getDishIngredient_Quantity();

	/**
	 * Returns the meta object for the reference '{@link com.restaurant.ingredientManagement.DishIngredient#getDishIngredient <em>Dish Ingredient</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Dish Ingredient</em>'.
	 * @see com.restaurant.ingredientManagement.DishIngredient#getDishIngredient()
	 * @see #getDishIngredient()
	 * @generated
	 */
	EReference getDishIngredient_DishIngredient();

	/**
	 * Returns the meta object for class '{@link com.restaurant.ingredientManagement.Dish <em>Dish</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Dish</em>'.
	 * @see com.restaurant.ingredientManagement.Dish
	 * @generated
	 */
	EClass getDish();

	/**
	 * Returns the meta object for the containment reference list '{@link com.restaurant.ingredientManagement.Dish#getDishIngredients <em>Dish Ingredients</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Dish Ingredients</em>'.
	 * @see com.restaurant.ingredientManagement.Dish#getDishIngredients()
	 * @see #getDish()
	 * @generated
	 */
	EReference getDish_DishIngredients();

	/**
	 * Returns the meta object for the attribute '{@link com.restaurant.ingredientManagement.Dish#getQuantity <em>Quantity</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Quantity</em>'.
	 * @see com.restaurant.ingredientManagement.Dish#getQuantity()
	 * @see #getDish()
	 * @generated
	 */
	EAttribute getDish_Quantity();

	/**
	 * Returns the meta object for the attribute '{@link com.restaurant.ingredientManagement.Dish#getNumberOfPeopleServed <em>Number Of People Served</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Number Of People Served</em>'.
	 * @see com.restaurant.ingredientManagement.Dish#getNumberOfPeopleServed()
	 * @see #getDish()
	 * @generated
	 */
	EAttribute getDish_NumberOfPeopleServed();

	/**
	 * Returns the meta object for the attribute '{@link com.restaurant.ingredientManagement.Dish#getName <em>Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Name</em>'.
	 * @see com.restaurant.ingredientManagement.Dish#getName()
	 * @see #getDish()
	 * @generated
	 */
	EAttribute getDish_Name();

	/**
	 * Returns the meta object for class '{@link com.restaurant.ingredientManagement.Restaurant <em>Restaurant</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Restaurant</em>'.
	 * @see com.restaurant.ingredientManagement.Restaurant
	 * @generated
	 */
	EClass getRestaurant();

	/**
	 * Returns the meta object for the attribute '{@link com.restaurant.ingredientManagement.Restaurant#getName <em>Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Name</em>'.
	 * @see com.restaurant.ingredientManagement.Restaurant#getName()
	 * @see #getRestaurant()
	 * @generated
	 */
	EAttribute getRestaurant_Name();

	/**
	 * Returns the meta object for the containment reference list '{@link com.restaurant.ingredientManagement.Restaurant#getDishes <em>Dishes</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Dishes</em>'.
	 * @see com.restaurant.ingredientManagement.Restaurant#getDishes()
	 * @see #getRestaurant()
	 * @generated
	 */
	EReference getRestaurant_Dishes();

	/**
	 * Returns the meta object for the containment reference list '{@link com.restaurant.ingredientManagement.Restaurant#getStockIngredients <em>Stock Ingredients</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Stock Ingredients</em>'.
	 * @see com.restaurant.ingredientManagement.Restaurant#getStockIngredients()
	 * @see #getRestaurant()
	 * @generated
	 */
	EReference getRestaurant_StockIngredients();

	/**
	 * Returns the meta object for the containment reference list '{@link com.restaurant.ingredientManagement.Restaurant#getIngredients <em>Ingredients</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Ingredients</em>'.
	 * @see com.restaurant.ingredientManagement.Restaurant#getIngredients()
	 * @see #getRestaurant()
	 * @generated
	 */
	EReference getRestaurant_Ingredients();

	/**
	 * Returns the meta object for the attribute '{@link com.restaurant.ingredientManagement.Restaurant#getGenerationDate <em>Generation Date</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Generation Date</em>'.
	 * @see com.restaurant.ingredientManagement.Restaurant#getGenerationDate()
	 * @see #getRestaurant()
	 * @generated
	 */
	EAttribute getRestaurant_GenerationDate();

	/**
	 * Returns the meta object for the attribute '{@link com.restaurant.ingredientManagement.Restaurant#getSlogan <em>Slogan</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Slogan</em>'.
	 * @see com.restaurant.ingredientManagement.Restaurant#getSlogan()
	 * @see #getRestaurant()
	 * @generated
	 */
	EAttribute getRestaurant_Slogan();

	/**
	 * Returns the meta object for enum '{@link com.restaurant.ingredientManagement.Measure <em>Measure</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for enum '<em>Measure</em>'.
	 * @see com.restaurant.ingredientManagement.Measure
	 * @generated
	 */
	EEnum getMeasure();

	/**
	 * Returns the factory that creates the instances of the model.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the factory that creates the instances of the model.
	 * @generated
	 */
	IngredientManagementFactory getIngredientManagementFactory();

	/**
	 * <!-- begin-user-doc -->
	 * Defines literals for the meta objects that represent
	 * <ul>
	 *   <li>each class,</li>
	 *   <li>each feature of each class,</li>
	 *   <li>each operation of each class,</li>
	 *   <li>each enum,</li>
	 *   <li>and each data type</li>
	 * </ul>
	 * <!-- end-user-doc -->
	 * @generated
	 */
	interface Literals {
		/**
		 * The meta object literal for the '{@link com.restaurant.ingredientManagement.impl.StockIngredientImpl <em>Stock Ingredient</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see com.restaurant.ingredientManagement.impl.StockIngredientImpl
		 * @see com.restaurant.ingredientManagement.impl.IngredientManagementPackageImpl#getStockIngredient()
		 * @generated
		 */
		EClass STOCK_INGREDIENT = eINSTANCE.getStockIngredient();

		/**
		 * The meta object literal for the '<em><b>Quantity</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute STOCK_INGREDIENT__QUANTITY = eINSTANCE.getStockIngredient_Quantity();

		/**
		 * The meta object literal for the '<em><b>Stock Ingredient</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference STOCK_INGREDIENT__STOCK_INGREDIENT = eINSTANCE.getStockIngredient_StockIngredient();

		/**
		 * The meta object literal for the '{@link com.restaurant.ingredientManagement.impl.IngredientImpl <em>Ingredient</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see com.restaurant.ingredientManagement.impl.IngredientImpl
		 * @see com.restaurant.ingredientManagement.impl.IngredientManagementPackageImpl#getIngredient()
		 * @generated
		 */
		EClass INGREDIENT = eINSTANCE.getIngredient();

		/**
		 * The meta object literal for the '<em><b>Name</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute INGREDIENT__NAME = eINSTANCE.getIngredient_Name();

		/**
		 * The meta object literal for the '<em><b>Calories</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute INGREDIENT__CALORIES = eINSTANCE.getIngredient_Calories();

		/**
		 * The meta object literal for the '<em><b>Calories Measure</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute INGREDIENT__CALORIES_MEASURE = eINSTANCE.getIngredient_CaloriesMeasure();

		/**
		 * The meta object literal for the '{@link com.restaurant.ingredientManagement.impl.DishIngredientImpl <em>Dish Ingredient</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see com.restaurant.ingredientManagement.impl.DishIngredientImpl
		 * @see com.restaurant.ingredientManagement.impl.IngredientManagementPackageImpl#getDishIngredient()
		 * @generated
		 */
		EClass DISH_INGREDIENT = eINSTANCE.getDishIngredient();

		/**
		 * The meta object literal for the '<em><b>Quantity</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute DISH_INGREDIENT__QUANTITY = eINSTANCE.getDishIngredient_Quantity();

		/**
		 * The meta object literal for the '<em><b>Dish Ingredient</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference DISH_INGREDIENT__DISH_INGREDIENT = eINSTANCE.getDishIngredient_DishIngredient();

		/**
		 * The meta object literal for the '{@link com.restaurant.ingredientManagement.impl.DishImpl <em>Dish</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see com.restaurant.ingredientManagement.impl.DishImpl
		 * @see com.restaurant.ingredientManagement.impl.IngredientManagementPackageImpl#getDish()
		 * @generated
		 */
		EClass DISH = eINSTANCE.getDish();

		/**
		 * The meta object literal for the '<em><b>Dish Ingredients</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference DISH__DISH_INGREDIENTS = eINSTANCE.getDish_DishIngredients();

		/**
		 * The meta object literal for the '<em><b>Quantity</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute DISH__QUANTITY = eINSTANCE.getDish_Quantity();

		/**
		 * The meta object literal for the '<em><b>Number Of People Served</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute DISH__NUMBER_OF_PEOPLE_SERVED = eINSTANCE.getDish_NumberOfPeopleServed();

		/**
		 * The meta object literal for the '<em><b>Name</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute DISH__NAME = eINSTANCE.getDish_Name();

		/**
		 * The meta object literal for the '{@link com.restaurant.ingredientManagement.impl.RestaurantImpl <em>Restaurant</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see com.restaurant.ingredientManagement.impl.RestaurantImpl
		 * @see com.restaurant.ingredientManagement.impl.IngredientManagementPackageImpl#getRestaurant()
		 * @generated
		 */
		EClass RESTAURANT = eINSTANCE.getRestaurant();

		/**
		 * The meta object literal for the '<em><b>Name</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute RESTAURANT__NAME = eINSTANCE.getRestaurant_Name();

		/**
		 * The meta object literal for the '<em><b>Dishes</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference RESTAURANT__DISHES = eINSTANCE.getRestaurant_Dishes();

		/**
		 * The meta object literal for the '<em><b>Stock Ingredients</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference RESTAURANT__STOCK_INGREDIENTS = eINSTANCE.getRestaurant_StockIngredients();

		/**
		 * The meta object literal for the '<em><b>Ingredients</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference RESTAURANT__INGREDIENTS = eINSTANCE.getRestaurant_Ingredients();

		/**
		 * The meta object literal for the '<em><b>Generation Date</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute RESTAURANT__GENERATION_DATE = eINSTANCE.getRestaurant_GenerationDate();

		/**
		 * The meta object literal for the '<em><b>Slogan</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute RESTAURANT__SLOGAN = eINSTANCE.getRestaurant_Slogan();

		/**
		 * The meta object literal for the '{@link com.restaurant.ingredientManagement.Measure <em>Measure</em>}' enum.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see com.restaurant.ingredientManagement.Measure
		 * @see com.restaurant.ingredientManagement.impl.IngredientManagementPackageImpl#getMeasure()
		 * @generated
		 */
		EEnum MEASURE = eINSTANCE.getMeasure();

	}

} //IngredientManagementPackage
