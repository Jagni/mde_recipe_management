/**
 */
package com.restaurant.ingredientManagement;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Ingredient</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link com.restaurant.ingredientManagement.Ingredient#getName <em>Name</em>}</li>
 *   <li>{@link com.restaurant.ingredientManagement.Ingredient#getCalories <em>Calories</em>}</li>
 *   <li>{@link com.restaurant.ingredientManagement.Ingredient#getCaloriesMeasure <em>Calories Measure</em>}</li>
 * </ul>
 *
 * @see com.restaurant.ingredientManagement.IngredientManagementPackage#getIngredient()
 * @model
 * @generated
 */
public interface Ingredient extends EObject {
	/**
	 * Returns the value of the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Name</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Name</em>' attribute.
	 * @see #setName(String)
	 * @see com.restaurant.ingredientManagement.IngredientManagementPackage#getIngredient_Name()
	 * @model
	 * @generated
	 */
	String getName();

	/**
	 * Sets the value of the '{@link com.restaurant.ingredientManagement.Ingredient#getName <em>Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Name</em>' attribute.
	 * @see #getName()
	 * @generated
	 */
	void setName(String value);

	/**
	 * Returns the value of the '<em><b>Calories</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Calories</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Calories</em>' attribute.
	 * @see #setCalories(int)
	 * @see com.restaurant.ingredientManagement.IngredientManagementPackage#getIngredient_Calories()
	 * @model
	 * @generated
	 */
	int getCalories();

	/**
	 * Sets the value of the '{@link com.restaurant.ingredientManagement.Ingredient#getCalories <em>Calories</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Calories</em>' attribute.
	 * @see #getCalories()
	 * @generated
	 */
	void setCalories(int value);

	/**
	 * Returns the value of the '<em><b>Calories Measure</b></em>' attribute.
	 * The default value is <code>"unit"</code>.
	 * The literals are from the enumeration {@link com.restaurant.ingredientManagement.Measure}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Calories Measure</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Calories Measure</em>' attribute.
	 * @see com.restaurant.ingredientManagement.Measure
	 * @see #setCaloriesMeasure(Measure)
	 * @see com.restaurant.ingredientManagement.IngredientManagementPackage#getIngredient_CaloriesMeasure()
	 * @model default="unit"
	 * @generated
	 */
	Measure getCaloriesMeasure();

	/**
	 * Sets the value of the '{@link com.restaurant.ingredientManagement.Ingredient#getCaloriesMeasure <em>Calories Measure</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Calories Measure</em>' attribute.
	 * @see com.restaurant.ingredientManagement.Measure
	 * @see #getCaloriesMeasure()
	 * @generated
	 */
	void setCaloriesMeasure(Measure value);

} // Ingredient
