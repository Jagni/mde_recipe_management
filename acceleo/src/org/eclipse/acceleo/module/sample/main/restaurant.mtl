[comment encoding = UTF-8 /]
[module restaurant('http://www.example.org/ingredientManagement')]

[template public generatePlanning(restaurant : Restaurant)]
[comment @main/]
[file ('stock.tex', false, 'UTF-8')]
\need{Stock (before)}

[for (stockIngredient : StockIngredient | stockIngredients)]
\FPeval{\result}{clip(0)}
\itemtag{[ stockIngredient.quantity /] [stockIngredient.stockIngredient.caloriesMeasure/]s of [stockIngredient.stockIngredient.name/]}
[/for]

\divider\smallskip

\need{Stock (after)}

[for (ingredient : Ingredient | ingredients) ]
\FPeval{\result}{clip(0[for (dish : Dish | restaurant.dishes)]+[dish.quantity/]*[dish.dishIngredients->select(c | c.dishIngredient.name = ingredient.name).quantity->sum() /][/for]-[ restaurant.stockIngredients->select(c | c.stockIngredient.name = ingredient.name).quantity.oclAsSet()->sum() /])}
\ifnum \result<0 \FPeval{\result}{clip(-\result)} \itemtag{\result~[ingredient.caloriesMeasure/]s of [ingredient.name/]}\fi
[/for]

\divider\smallskip
[/file]

[file ('main.tex', false, 'UTF-8')]
\documentclass['['/]10pt,a4paper]{protocol}

% Change the page layout if you need to
\geometry{left=1cm,right=9cm,marginparwidth=6.8cm,marginparsep=1.2cm,top=1cm,bottom=1cm}

% Change the font if you want to.

% If using pdflatex:
\usepackage['['/]nomessages]{fp}
\usepackage['['/]utf8]{inputenc}
\usepackage['['/]T1]{fontenc}
\usepackage['['/]default]{lato}

% If using xelatex or lualatex:
% \setmainfont{Lato}

% Change the colours if you want to
\definecolor{Bright}{HTML}{cd2c5a}
\definecolor{Green}{HTML}{d3a06c}
\definecolor{Black}{HTML}{111111}
\definecolor{LightGrey}{HTML}{515c50}
\colorlet{heading}{Green}
\colorlet{accent}{Bright}
\colorlet{emphasis}{Black}
\colorlet{body}{LightGrey}

% Change the bullets for itemize and rating marker
% for \risk if you want to
\renewcommand{\itemmarker}{{\small\textbullet}}
\renewcommand{\ratingmarker}{\faSpinner}

%% sample.bib contains your publications
\addbibresource{sample.bib}

\begin{document}
\name{[restaurant.name/]}
\tagline{[restaurant.slogan/]}
\made{October 2 }
\logo{6.5cm}{"Logo"}


\docinfo{%
  % can add more \addedtopeople
  \madeby{Jagni Dasa}{abv1@uni.ac.uk}{[restaurant.generationDate/]}
}


\purpose{
	A stock report to help manage [restaurant.name/]'s ingredients and dishes.
} % add a short discription of the purpose for this protocol


%% Make the header extend all the way to the right, if you want.
\begin{fullwidth}
\makeheader
\end{fullwidth}

%% Provide the file name containing the sidebar contents as an optional parameter to \need.
%% You can always just use \marginpar{...} if you do
%% not need to align the top of the contents to any
%% \need title in the "main" bar.
\need['['/]stock]{Shopping List}

[let restaurant : Restaurant = self]

\begin{itemize}
[for (ingredient : Ingredient | ingredients) ]
\FPeval{\result}{clip(0[for (dish : Dish | restaurant.dishes)]+[dish.quantity/]*[dish.dishIngredients->select(c | c.dishIngredient.name = ingredient.name).quantity->sum() /][/for]-[ restaurant.stockIngredients->select(c | c.stockIngredient.name = ingredient.name).quantity.oclAsSet()->sum() /])}
\ifnum \result<0 \else \item You need to buy \result~[ingredient.caloriesMeasure/]s of [ingredient.name/]\fi
[/for]
\end{itemize}

\need['['/]references]{Planned Dishes}

[for (dish : Dish | dishes)]
[let dishQuantity : Integer = dish.quantity]
\step{[ restaurant.dishes.oclAsSet()->indexOf(dish) /]}{[ dish.quantity /] [ dish.name /]}{[ dish.numberOfPeopleServed*dish.quantity /]}
\begin{itemize}
	[for (dishIngredient : DishIngredient | dishIngredients)]
	[let ingredient : Ingredient = dishIngredient.dishIngredient ]
	
	[if (dishQuantity*dishIngredient.quantity <= 1)]
	\item [ dishQuantity*dishIngredient.quantity + ' ' + ingredient.caloriesMeasure + ' of ' +  ingredient.name/]
	[/if]
	[if (dishQuantity*dishIngredient.quantity > 1)]
	\item [ dishQuantity*dishIngredient.quantity + ' ' + ingredient.caloriesMeasure + 's of ' +  ingredient.name/]
	[/if]
	[/let]
	[/for]
	\item \FPeval{\result}{clip(0[for (dishIngredient : DishIngredient | dishIngredients)]+[dishIngredient.quantity/]*[dishIngredient.dishIngredient.calories /][/for])} Total of \result kcal per dish
\end{itemize}
\divider
[/let]
[/for]

[/let]


%% If the NEXT page doesn't start with a \need but you'd
%% still like to add a sidebar, then use this command on THIS
%% page to add it. The optional argument lets you pull up the
%% sidebar a bit so that it looks aligned with the top of the
%% main column.
% \addnextpagesidebar['['/]-1ex]{page3sidebar}


\end{document}

[/file]
[/template]


